import Foundation

let numberOfNeededOfflineContactServers = 10

public class Client {
    public let address =  NSUUID().uuidString
    let forwardingServer: ForwardingServer
    let messagingService: MessagingService
    var friends: Set<Client> = []
    var friendsOfflineContactDetails: [Client: Set<OfflineContactServer>] = [:]
    var knownForwardingServers: Set<ForwardingServer> = [] {
        didSet {
            //TODO: Check that is actually works with change
        }
    }
    var currentlySelectedForwardingServers: Set<OfflineDownloadServer> = []
    var isOnline = false

    private func add(friend: Client) {
        friends.update(with: friend)
    }

    private func sendAsyncContactDetails(to client: Client) {
        guard !currentlySelectedForwardingServers.isEmpty else { return }
        client.recieveOfflineContact(details: Set(currentlySelectedForwardingServers.map(OfflineContactServer.init(server:))), from: self)
    }

    private func addSelfForwardingServer() {
        self.knownForwardingServers.insert(self.forwardingServer)
        let serviceToken = self.forwardingServer.requestFowardingServices()
        self.currentlySelectedForwardingServers.insert(OfflineDownloadServer(forwardingServer: self.forwardingServer, uploadToken: serviceToken.uploadToken, downloadToken: serviceToken.downloadToken))
    }

    init(forwardingServer: ForwardingServer, messagingService: MessagingService) {
        self.forwardingServer = forwardingServer
        self.messagingService = messagingService
        addSelfForwardingServer()
    }
}

extension Client: THICCSender {
    public func request(friend: Client) {
        let result = friend.handleFriendRequest(from: self)
        switch result {
        case .ack(true): add(friend: friend)
        default: break
        }
    }
}

extension Client: THICCReceiver {
    public func handleFriendRequest(from requester: Client) -> Result<Bool> {
        guard isOnline else { return .nack(error:"offline") }
        guard !friends.contains(requester) else { return .ack(true) }
        add(friend: requester)
        return .ack(true)
    }

    public func recieveOfflineContact(details: Set<OfflineContactServer>,from friend: Client) {
        guard friends.contains(friend) else { return }
        friendsOfflineContactDetails[friend] = details
    }
}

extension Client: Hashable {
    public static func ==(lhs: Client, rhs: Client) -> Bool {
        return lhs.address == rhs.address
    }

    public var hashValue: Int {
        return address.hashValue
    }
}
