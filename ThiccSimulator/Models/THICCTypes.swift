import Foundation

public typealias UUIDString = String
public typealias MessageUploadToken = String
public typealias MessageUploadReceipt = String
public typealias MessageDownloadToken = String

public struct ServiceToken {
    let uploadToken: MessageUploadToken
    let downloadToken: MessageDownloadToken
}

public enum Result<T> {
    case ack(T)
    case nack(error: String)
}

public struct Message {
    let createdAt: Date
    let content: String
    let _sender: Client //This is for convenience. Is implemented with a session cookie and mapping the protocol
}

public struct ForwardMessage {
    let uploadToken: MessageUploadToken
    let message: [Message]
}

public struct OfflineContactServer: Hashable {
    let forwardingServer: ForwardingServer
    let uploadToken: MessageUploadToken

    public var hashValue: Int {
        return forwardingServer.hashValue //We want this behaviour so our set replaces old contacts
    }

    public static func ==(lhs: OfflineContactServer, rhs: OfflineContactServer) -> Bool {
        guard lhs.forwardingServer == rhs.forwardingServer else { return false }
        guard lhs.uploadToken == rhs.uploadToken else { return false }
        return true
    }
}

extension OfflineContactServer {
    init(server: OfflineDownloadServer) {
        self.forwardingServer = server.forwardingServer
        self.uploadToken = server.uploadToken
    }
}

public struct OfflineDownloadServer: Hashable {
    let forwardingServer: ForwardingServer
    let uploadToken: MessageUploadToken
    let downloadToken: MessageUploadToken

    public var hashValue: Int {
        return forwardingServer.hashValue //We want this behaviour so our set replaces old contacts
    }

    public static func ==(lhs: OfflineDownloadServer, rhs: OfflineDownloadServer) -> Bool {
        guard lhs.forwardingServer == rhs.forwardingServer else { return false }
        guard lhs.uploadToken == rhs.uploadToken else { return false }
        guard lhs.downloadToken == rhs.downloadToken else { return false }
        return true
    }
}
