import Foundation

public class ForwardingServer: Hashable {
    private unowned let client: Client
    public let publicAddress =  NSUUID().uuidString
    var messageQueue: [MessageUploadToken: [Message]] = [:]
    var registeredTokens: [ServiceToken] = []

    var isOnline: Bool {
        return client.isOnline
    }

    init(client: Client) {
        self.client = client
    }

    private func generateToken() -> ServiceToken {
        return ServiceToken(uploadToken: NSUUID().uuidString,
                            downloadToken: NSUUID().uuidString)
    }

    func requestFowardingServices() -> ServiceToken {
        let token = generateToken()
        registeredTokens.append(token)
        return token
    }

    func upload(message: ForwardMessage) -> Result<MessageUploadReceipt> {
        let uploadTokens = registeredTokens.map { $0.uploadToken }
        guard uploadTokens.contains(message.uploadToken) else { return .nack(error: "Token not registered") }
        var currentMessages = messageQueue[message.uploadToken] ?? []
        currentMessages.append(message.message)
        messageQueue[message.uploadToken] = currentMessages
        return .ack("TEST")
    }

    func downloadMessages(for downloadToken: MessageDownloadToken) -> [Message] {
        let token = registeredTokens.first(where: {$0.downloadToken == downloadToken})
        guard let uploadToken = token?.uploadToken,
            let messages = messageQueue[uploadToken] else { return [] }
        messageQueue.removeValue(forKey: uploadToken)
        return messages
    }

    public static func == (lhs: ForwardingServer, rhs: ForwardingServer) -> Bool {
        return lhs.publicAddress == rhs.publicAddress
    }

    public var hashValue: Int {
        return self.publicAddress.hashValue
    }
}
