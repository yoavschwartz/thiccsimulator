import Foundation

public protocol THICCReceiver {
    func handleFriendRequest(from requester: Client) -> Result<Bool>
    func recieveOfflineContact(details: Set<OfflineContactServer>,from friend: Client)
}

public protocol THICCSender {
    var address: UUIDString { get }
    func request(friend: Client)
}
