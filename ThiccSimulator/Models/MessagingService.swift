//
//  MessagingService.swift
//  ThiccSimulator
//
//  Created by Yoav Schwartz on 31/03/2018.
//  Copyright © 2018 Thunder Apps. All rights reserved.
//

import Foundation

private let NumberOfForwardingServersToUse = 3
private let MessageWindowSize = 3

class MessagingService {
    private unowned let client: Client
//    var unackedSentMessages: [Client: [Message]] = [:]
//    var messagesToAcknowledge: [Client: [Message]] = [:]

    init(client: Client) {
        self.client = client
    }

    func sendMessage(_ message: Message, to friend: Client ) -> Bool {
        if friend.isOnline {
            friend.messagingService.receiveMessage(message)
            return true
        } else {
            let didSend = self.sendAsyncMessage(message, to: friend)
            if didSend {
                //Maybe TODO:
//                unackedSentMessages[friend] = (unackedSentMessages[friend] ?? []) + [message]
            }
            return didSend
        }
    }

    private func sendAsyncMessage(_ message: Message, to friend: Client) -> Bool {
        let optOfflineContacts = client.friendsOfflineContactDetails
            .first(where: { (client, servers) -> Bool in
                client == friend
            })?.value

        guard let offlineContacts = optOfflineContacts,
            !offlineContacts.isEmpty else { return false }

        var sentCount = 0

        for offlineContact in offlineContacts {
            if offlineContact.forwardingServer.isOnline &&
                sentCount < NumberOfForwardingServersToUse {
                let fowardMessage = ForwardMessage(uploadToken: offlineContact.uploadToken, message: message)
                let result = offlineContact.forwardingServer.upload(message: fowardMessage)
                switch result {
                case .ack: sentCount += 1
                case .nack: break
                }
            }
        }
        let didSend = sentCount > 0
        return didSend
    }

    private func retrieveAsyncsMessages() {
        for asyncServer in client.currentlySelectedForwardingServers {
            if asyncServer.forwardingServer.isOnline {
                let messages = asyncServer.forwardingServer.downloadMessages(for: asyncServer.downloadToken)
                //TODO: Something
            }
        }
    }

    public func receiveMessage(_ message: Message) {
        //TODO: Do something with message
    }
}
